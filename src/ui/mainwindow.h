//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
/**
 * @brief The MainWindow class
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    /**
     * @brief button
     */
    QPushButton *button;

public:
    /**
     * @brief MainWindow
     * @param parent
     */
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
};

#endif // MAINWINDOW_H
