//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "testwindow.h"
#include "ui_testwindow.h"

#include <QPushButton>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QSizePolicy>
#include <QFontDatabase>
#include <QtDebug>
#include <model/gamemanager.h>
#include <model/game.h>
#include <model/field.h>
#include <QTimer>

void testwindow::setupgrid(){
    QFontDatabase::addApplicationFont(":/fonts/chess.ttf");
    //QSizePolicy pol = QSizePolicy();
    //pol.setVerticalPolicy(QSizePolicy::Expanding);
    //pol.setHorizontalPolicy(QSizePolicy::Expanding);
    //pol.setHorizontalStretch(1);
    //pol.setVerticalStretch(1);
    for (int i = 0; i < 8; i++){
        QLabel *l = new QLabel(QString::number(8 - i));
        //QLabel *l = new QLabel(QString::number(7 - i));
        ui->cisla_sloupcu->addWidget(l);
        l = new QLabel(QString('a' + i));
        //l = new QLabel(QString::number(i));
        l->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
        ui->cisla_radku->addWidget(l);
    }
    for (int i = 0; i < 8; i++){
        for (int j = 0; j < 8; j++){
            //Pridavani tlacitek pro hraci desku
            QPushButton *b = new QPushButton(ui->grid);
            b->setText(" ");
            b->setFont(chessFont);
            //b->setSizePolicy(pol);
            b->setCheckable(true);
            b->setProperty("chessbutton", true);
            if (i % 2 == 0){
                if (j % 2 == 0){
                    b->setProperty("black", true);
                } else {
                    b->setProperty("black", false);
                }
            } else {
                if (j%2 == 1){
                    b->setProperty("black", true);
                } else {
                    b->setProperty("black", false);
                }
            }
            b->setProperty("index", (7 - i) * 8 +j);
            ui->gridLayout->addWidget(b, i, j);
            QObject::connect(b, SIGNAL(clicked()), this, SLOT(onGridClicked()));
            gridButtons[(7 - i) * 8 +j] = b;
        }

    }
    ui->gridLayout->update();
    //Je potreba pro znovu vykresleni hraci desky
    ui->centralwidget->style()->unpolish(ui->centralwidget);
    ui->centralwidget->style()->polish(ui->centralwidget);
}

void testwindow::drawUndos(Game *game){
    ui->undos->clear();
    for (auto& m : game->getMoves()){
        ui->undos->addItem(QString::fromStdString(m.getFullNotation()));
    }
    ui->undos->setItemSelected(ui->undos->item(game->getCurrentMove()), true);
}

void testwindow::drawGame(Game *game)
{
    ui->grid->setEnabled(true);
    for (int i = 0; i < 8; i++){
        for (int j = 0; j < 8; j++){
            Field *f = game->getBoard()->getCell(i, j);
            QPushButton *button = gridButtons[i * 8 +j];
            if (f->hasPiece()){
                Piece *p = f->getPiece();
                button->setText(QString(p->GetPieceChar()));
            } else {
                button->setText(" ");
            }
        }
    }
    drawUndos(game);
}


testwindow::testwindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::testwindow)
{
    timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(onTimer()));
    activeButton = nullptr;
    chessFont = QFont("Chess-7", 60, 0, false);
    ui->setupUi(this);
    //Nastaveni prehravaci rychlosti na polovinu maximalni
    ui->horizontalSlider->setValue(ui->horizontalSlider->maximum() / 2);
    setupgrid();
}

testwindow::~testwindow()
{
    delete timer;
    delete ui;
}

void testwindow::on_pushButton_clicked()
{
    Game * game = GameManager::getManager()->getCurrentGame();
    game->undo();
    drawGame(game);
}

void testwindow::onGridClicked() {
    QObject *s = sender();
    for (int i = 0; i < 64; i++){
        //Prekresleni hraci desky
        QPushButton *b = gridButtons[i];
        b->setChecked(false);
        if (!b->property("peace").isNull()){
            b->setProperty("peace", false);
            ui->centralwidget->style()->unpolish(b);
            ui->centralwidget->style()->polish(b);
        }
        if (!b->property("kill").isNull()){
            b->setProperty("kill", false);
            ui->centralwidget->style()->unpolish(b);
            ui->centralwidget->style()->polish(b);
        }
    }
    Game *game = GameManager::getManager()->getCurrentGame();
    //Zisk aktualniho pole
    QPushButton *but = qobject_cast<QPushButton*>(s);
    int index = but->property("index").toInt();
    Field *f = game->getBoard()->getCell(index);
    if (activeButton == nullptr){
        if (f->hasPiece() && f->getPiece()->isBlack() != game->isBlackTurn()){
            return;
        }
        //Vyber prvniho pole
        but->setChecked(true);
        activeButton = but;
        if (f->hasPiece()){
            auto moves = f->getPiece()->allMoves();
            for (auto& m : moves){
                //Vykresleni pripustnych pohybu
                QPushButton *b = gridButtons[m.address.row * 8 + m.address.col];
                if (m.type == PEACE){
                    b->setProperty("peace", true);
                } else {
                    b->setProperty("kill", true);
                }
                ui->centralwidget->style()->unpolish(b);
                ui->centralwidget->style()->polish(b);
            }
        }
    } else {
        //Vyber druheho pole -> pravdepodobne skok
        int fromIndex = activeButton->property("index").toInt();
        Field *fromField = game->getBoard()->getCell(fromIndex);
        if (fromField->hasPiece()){
            if (f->hasPiece()){
                if (f->getPiece()->isBlack() == game->isBlackTurn()){
                    drawGame(game);
                    activeButton = nullptr;
                    onGridClicked();
                    return;
                }
            }
            if (fromField->getPiece()->canMove(f->getAddress())){
                game->doMove(Move::fromTo(fromField, f));
            }

        }
        activeButton = nullptr;
        drawGame(game);
    }

}

void testwindow::onTimer()
{
    Game *game = GameManager::getManager()->getCurrentGame();
    //2 = checked
    if (ui->Rewind->checkState() == 2){
        game->undo();
    } else {
        game->redo();
    }
    drawGame(game);
}

#include <QResizeEvent>

void testwindow::resizeEvent(QResizeEvent *event)
{

}

void testwindow::on_undos_itemClicked(QListWidgetItem *item)
{
    Game *game = GameManager::getManager()->getCurrentGame();
    int index = ui->undos->row(item);
    game->setCurrentMove(index);
    drawGame(game);
}

void testwindow::on_undos_currentRowChanged(int currentRow)
{
    //GameManager::getManager()->getCurrentGame()->setCurrentMove(currentRow);
}

void testwindow::on_pushButton_2_clicked()
{
    Game *game = GameManager::getManager()->getCurrentGame();
    game->redo();
    drawGame(game);
}

void testwindow::on_pushButton_3_clicked()
{
    if (timer->isActive()){
        timer->stop();
        ui->horizontalSlider->setEnabled(true);
        ui->pushButton_3->setText("Play");
    } else {
        timer->setInterval(ui->horizontalSlider->value());
        timer->start();
        ui->horizontalSlider->setEnabled(false);
        ui->pushButton_3->setText("Pause");
    }

}

void testwindow::on_horizontalSlider_valueChanged(int value)
{
    QString s;
    s.sprintf("Rychlost prehravani: %d ms", value);
    ui->lPlayback_speed->setText(s);
}

#include <QFileDialog>

void testwindow::on_pushButton_6_clicked()
{
    QFileDialog dialog;
    int status = dialog.exec();
    if (status == 0)
        return;
    try {
        int id;
        std::string filepath = dialog.selectedFiles().first().toStdString();
        Game* newGame = GameManager::getManager()->createGame(id);
        newGame->initialize(filepath);
        GameManager::getManager()->setCurrentGame(id);
        drawGame(newGame);
        QString gameName;
        std::string filename = filepath.substr(filepath.find_last_of('/'));
        gameName.sprintf("%d - %s", id, filename.c_str());
        ui->gameList->addItem(gameName);
    } catch (char const *e) {
        QMessageBox mb;
        mb.setText(QString::fromStdString(e));
        mb.exec();
    }
}

void testwindow::on_pushButton_5_clicked()
{
    int id;
    Game* newGame = GameManager::getManager()->createGame(id);
    GameManager::getManager()->setCurrentGame(id);
    drawGame(newGame);
    QString gameName;
    gameName.sprintf("%d <Bez nazvu>", id);
    ui->gameList->addItem(gameName);
}

void testwindow::on_gameList_itemClicked(QListWidgetItem *item)
{
    int id = ui->gameList->row(item);
    GameManager::getManager()->setCurrentGame(id);
    drawGame(GameManager::getManager()->getCurrentGame());
}


void testwindow::on_pushButton_4_clicked()
{
    Game *game = GameManager::getManager()->getCurrentGame();
    std::ofstream output;
    QFileDialog dialog;
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    int status = dialog.exec();
    if (status == 0)
        return;
    output.open(dialog.selectedFiles().first().toStdString());
    game->saveGame(output);
    output.close();
}
