//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "game.h"

#include <iostream>
#include <QDebug>

Game::Game() : board(this){
    currentMove = -1;
    isBlacksTurn = false;

}

void Game::initialize(const std::string &filepath){
    Parser parser(&this->board, filepath);
    while (parser.hasNext()){
        std::pair<Move, Move> m = parser.nextMove();
        if (m.first.isValid()){
            if (board.isMoveValid(m.first)){
                doMove(m.first);
            } else {
                throw "Invalidni pohyb ";
            }
        }
        if (m.second.isValid()){
            if (board.isMoveValid(m.second)){
                doMove(m.second);
            } else {
                throw "Invalidni pohyb ";
            }
        }
    }
}

void Game::doMove(Move move)
{
    if (isBlacksTurn){
        if (!board.getCell(move.getFrom())->getPiece()->isBlack()){
            return;
        }
    } else {
        if (board.getCell(move.getFrom())->getPiece()->isBlack()){
            return;
        }
    }
    isBlacksTurn = !isBlacksTurn;
    currentMove++;
    while (currentMove != moves.size()){
        moves.pop_back();
    }
    moves.push_back(move);
    Field *from = getBoard()->getCell(move.getFrom());
    Field *to = getBoard()->getCell(move.getTo());
    to->setPiece(from->getPiece());
    from->setPiece(nullptr);
    qDebug() << currentMove;
}

void Game::undo()
{
    if (currentMove < 0){
        return;
    }
    Move m = moves[currentMove];
    Field *from = getBoard()->getCell(m.getFrom());
    Field *to = getBoard()->getCell(m.getTo());
    from->setPiece(to->getPiece());
    to->setPiece(m.getPiece());
    currentMove--;
    isBlacksTurn = !isBlacksTurn;
}

void Game::redo()
{
    if (currentMove + 1 >= moves.size()){
        return;
    }
    currentMove++;
    Move m = moves[currentMove];
    Field *from = getBoard()->getCell(m.getFrom());
    Field *to = getBoard()->getCell(m.getTo());
    to->setPiece(from->getPiece());
    from->setPiece(nullptr);
    isBlacksTurn = !isBlacksTurn;
}

Board *Game::getBoard()
{
    return &this->board;
}

std::vector<Move> Game::getMoves()
{
    return moves;
}

std::vector<Piece *> Game::getAllPieces(bool getBlack) {
    std::vector<Piece *> pieces;
    for (int i = 0; i<8; i++) {
        for (int j = 0; j<8; j++) {
            if (!(this->board.getCell(i,j)->isEmpty())) {
                if (getBlack == this->board.getCell(i,j)->getPiece()->isBlack()){
                    pieces.push_back(this->board.getCell(i,j)->getPiece());
                }
            }
        }
    }
    return pieces;
}

std::vector<all_moves> Game::getAllPossibleMoves(bool fromBlack) {
    std::vector<Piece *> pieces = getAllPieces(fromBlack);
    std::vector<all_moves> all_moves, moves_of_one_piece;

    for (auto& p : pieces){
        if (p->getType() == PieceType::KRAL) {
            moves_of_one_piece = p->allMoves();
            std::copy(all_moves.begin(), all_moves.end(), std::back_inserter(moves_of_one_piece));
        }
    }
    return all_moves;
}


std::vector<Address> Game::getAllReachableFields(bool ofBlack) {
    // TODO pozor negace.
    std::vector<all_moves> all_fields = getAllPossibleMoves(ofBlack);
    std::vector<Address> reachable_fields;

    for (auto& r: all_fields) {
        if (r.type == PEACE) {
            reachable_fields.push_back(r.address);
        }
    }

    return reachable_fields;
}

std::vector<Address> Game::getRedFileds(bool ofBlack) {
    // TODO pozor negace.
    std::vector<all_moves> all_fields = getAllPossibleMoves(ofBlack);
    std::vector<Address> red_fields;

    for (auto& r: all_fields) {
        if (r.type == KILL) {
            red_fields.push_back(r.address);
        }
    }

    return red_fields;
}

Piece* Game::getKing(bool getBlack) {
    std::vector<Piece *> all_pieces = getAllPieces(getBlack);

    for (auto& r: all_pieces) {
        if (r->getType() == KRAL) {
            return r;
        }
    }
    return nullptr;
}

bool Game::sach(bool isPlayingBlack) {
    Piece *king = getKing(isPlayingBlack);
    std::vector<Address> red_fields = getRedFileds(!isPlayingBlack);
    for (auto& r: red_fields) {
        if (r == king->getAddress()) {
            return true;
        }
    }
    return false;
}

void Game::saveGame(std::ofstream& out)
{
    int size = moves.size();
    size -= size % 2 == 0? 0:1;
    for (int i = 0; i < size; i+=2){
        QString s;
        s.sprintf("%d. %s %s\n", i / 2 + 1, moves[i].getFullNotation().c_str(), moves[i+1].getFullNotation().c_str());
        out << s.toStdString();
    }
    if (moves.size() % 2 == 1){
        QString s;
        int moveNum = moves.size() / 2;
        if (moveNum == 0){
            moveNum = 1;
        }
        s.sprintf("%d. %s\n", moveNum, moves.back().getFullNotation().c_str());
        out << s.toStdString();
    }
}

int Game::getCurrentMove()
{
    return currentMove;
}

bool Game::isBlackTurn()
{
    return isBlacksTurn;
}

void Game::setCurrentMove(int move)
{
    if (move < currentMove){
        while (currentMove != move){
            undo();
        }
    } else if (move > currentMove){
        while (currentMove != move){
            redo();
        }
    }
    currentMove = move;
}
