//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "dama.h"

/**
 * @brief Pomocná věž při kontrole pohybu
 */
Vez Dama::stVez = Vez(false, nullptr);
/**
 * @brief Pomocný střelec při kontrole pohybu
 */
Strelec Dama::stStrelec = Strelec(false, nullptr);

/**
 * @brief Zjišťuje validitu tahu na dané pole
 * @details Dáma se pohybuje jako věž a střelec zároveň, od toho se taky odvíjí tato funkce, která jen sloučí výstupy funkce canMove pro Věž a pro Střelce.
 * @param address pole kam chci jít
 * @return True, pokud by byl tah validní
 */
bool Dama::canMove(Address address) {
    Dama::stVez.setAddress(this->address);
    Dama::stStrelec.setAddress(this->address);
    Dama::stVez.setIsBlack(this->m_isBlack);
    Dama::stVez.setBoard(board);
    Dama::stStrelec.setBoard(board);
    return Dama::stVez.canMove(address) || Dama::stStrelec.canMove(address);
    // strelec or vez
}

/**
 * @brief Zjišťuje, kam všude dáma může jít.
 * @details Sloučí výstup allMoves od Střelce a od Věže, které jsou virtuálně dosazeny na dané pole.
 * @return Vrací všechna pole, kam Dáma může aktuálně vstoupit.
 */
std::vector<all_moves> Dama::allMoves() {
    Dama::stVez.setAddress(address);
    Dama::stStrelec.setAddress(address);
    Dama::stVez.setBoard(board);
    Dama::stStrelec.setBoard(board);
    std::vector<all_moves> movesVez = Dama::stVez.allMoves();
    std::vector<all_moves> movesStrelec = Dama::stStrelec.allMoves();
    std::vector<all_moves> movesDama(movesVez);

    std::copy(movesStrelec.begin(), movesStrelec.end(), std::back_inserter(movesDama));
    //todo;
    // strelec or vez

    return movesDama;
}

PieceType Dama::getType() {
    return PieceType::DAMA;
}

char Dama::GetPieceChar() {
    if (this->isBlack()) {
        return 'w';
    }
    return 'q';
}

void Dama::setIsBlack (bool isBlack) {
    this->m_isBlack = isBlack;
}
void setIsBlack(bool isBlack);
