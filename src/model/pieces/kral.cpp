//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "kral.h"
#include "model/board.h"
#include <set>
#include <vector>
#include <model/game.h>
#include <model/gamemanager.h>


bool Kral::canMove(Address address) {
    int i = address.row - this->getRow();
    int j = address.col - this->getCol();

    if ((i == 0) && (j == 0)) {
        return false;
    }

    std::set <int> m = {-1,0,1};
    if (!((m.count(i)) && (m.count(j)))) {
        return false;
    }

    int x = address.row;
    int y = address.col;

    if (x < 0 || x > 7 || y < 0 || y > 7) {
        return false;
    }

    if (!(this->getField(x,y)->isEmpty())) {
        // budu brat (mozna)
        if (!(this->getField(x,y)->isEmpty())){
            if (this->isBlack()) {
                if (this->getField(x,y)->getPiece()->isBlack()) {
                    return false;
                }
            } else {
                if (!(this->getField(x,y)->getPiece()->isBlack())) {
                    return false;
                }
            }
        }
    }
    // tah by vedl do sachu
    Game* game = GameManager::getManager()->getCurrentGame();
    //std::vector<Address> red_fields = game->getRedFileds(!(this->isBlack()));

//    for (auto& r: red_fields) {
//        if (r == address) {
//            return false;
//        }
//    }

    return true;
}

std::vector<all_moves> Kral::allMoves() {
    std::vector<all_moves> moves;
    all_moves one_move;
    for (int i = -1; i<=1; i++) {
        for (int j = -1; j<=1; j++) {
            if ((i == 0) && (j == 0)) {
                continue;
            }
            int x = this->getRow() + i;
            int y = this->getCol() + j;
            if (x < 0 || x > 7 || y < 0 || y > 7) {
                continue;
            }
            one_move.address.row = x;
            one_move.address.col = y;
            if (this->canMove(one_move.address)) {
                one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
                moves.push_back(one_move);
            }
        }
    }
    return moves;
}

PieceType Kral::getType() {
    return PieceType::KRAL;
}

char Kral::GetPieceChar() {
    if (this->isBlack()) {
        return 'l';
    }
    return 'k';
}

