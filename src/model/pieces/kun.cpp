//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "kun.h"
#include "model/board.h"
#include <cmath>

bool Kun::canMove(Address address) {
    int i = address.row - this->getRow();
    int j = address.col - this->getCol();

    // ___    |
    //  |  a ---
    if (!((abs(i) == 2) && (abs(j) == 1))) {
        // |-- a --|
        if (!((abs(i) == 1) && (abs(j) == 2))) {
            return false;
        }
    }



    int x = address.row;
    int y = address.col;

    if (x < 0 || x > 7 || y < 0 || y > 7) {
        return false;
    }

    // budu brat (mozna)
    if (!(this->getField(x,y)->isEmpty())){
        if (this->isBlack()) {
            if (this->getField(x,y)->getPiece()->isBlack()) {
                return false;
            }
        } else {
            if (!(this->getField(x,y)->getPiece()->isBlack())) {
                return false;
            }
        }
    }

    return true;
}


std::vector<all_moves> Kun::allMoves() {
    std::vector<all_moves> moves;
    all_moves one_move;
    for (int i = -2; i <= 2; i++) {
        for (int j = -2; j <= 2; j++) {
            std::cout << "i =" << i << " j =" << j << " | ";
                        // filtr vnitni cverec kolem kone
            //if (((abs(i) == abs(1)) && (abs(j) == 1)) || ((i == 0) && (j == 0)) ||
            //        // filtr diagonalne a horizontalne
            //        ((abs(i) == 2) && (j == 0)) || ((i == 0) && (abs(j) == 2)) ||
            //       // filtr rohy
            //        ((abs(i) == 2) && (abs(j) == 2)) ) {
            //    continue;
            //}

            if (((abs(i) == 2) && (abs(j) == 1)) || ((abs(i) == 1) && (abs(j) == 2))) {
                int x = this->getRow() + i;
                int y = this->getCol() + j;
                std::cout << "x =" << this->getRow() << "->" << x << " y =" << this->getCol() << "->" << y << std::endl;
                if (x >= 0 && x <= 7 && y >= 0 && y <= 7) {
                    one_move.address.row = x;
                    one_move.address.col = y;
                    if (this->canMove(one_move.address)) {
                        one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
                        moves.push_back(one_move);
                        std::cout << " <<<<<<<<< cajk";
                    }
                    std::cout << std::endl;
                }
            }
        }
    }
    return moves;
}

PieceType Kun::getType() {
    return PieceType::KUN;
}

char Kun::GetPieceChar() {
    if (this->isBlack()) {
        return 'm';
    }
    return 'n';
}
