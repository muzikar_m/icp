//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef DAMA_H
#define DAMA_H

#include <QObject>
#include <QWidget>
#include <model/piece.h>
#include <model/pieces/strelec.h>
#include <model/pieces/vez.h>

class Dama : public Piece {
public:
    /**
     * @brief Pomocná věž pro pohyb dámy
     */
    static Vez stVez;
    /**
     * @brief Pomocný střelec pro pohyb dámy
     */
    static Strelec stStrelec;
    using Piece::Piece;
    /**
     * @brief Ověří validitu pohybu dámy
     * @param address adresa, kam se chci přesunout
     * @return True pokud je to validní pohyb, jinak false
     */
    bool canMove(Address address) override;
    /**
     * @brief Vrací typ dané figurky
     * @return typ figurky: dáma
     */
    PieceType getType() override;
    /**
     * @brief Vrací znak ve fontu, který značí dámu
     * @return znak dámy
     */
    char GetPieceChar() override;
    /**
     * @brief Zjistí, kam se všude může dáma aktuálně pohybovat.
     * @return pole všech pohybů
     */
    std::vector<all_moves> allMoves() override;
    /**
     * @brief Nastavuje barvu dámy
     * @param isBlack   True - černá, False - bílá
     */
    void setIsBlack(bool isBlack);
};

#endif // DAMA_H
