//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef VEZ_H
#define VEZ_H

#include <QObject>
#include <QWidget>
#include <model/piece.h>

class Vez : public Piece {
public:
    using Piece::Piece;
    /**
     * @brief Ověří validitu pohybu věže
     * @param address adresa, kam se chci přesunout
     * @return True pokud je to validní pohyb, jinak false
     */
    bool canMove(Address address) override;
    /**
     * @brief Vrací typ dané figurky
     * @return typ figurky: věž
     */
    PieceType getType() override;
    /**
     * @brief Vrací znak ve fontu, který značí vež
     * @return znak vež
     */
    char GetPieceChar() override;
    /**
     * @brief Zjistí, kam se všude může věž aktuálně pohybovat.
     * @return pole všech pohybů
     */
    std::vector<all_moves> allMoves() override;
    /**
     * @brief Nastavuje barvu věže
     * @details Je to potřeba kvůli adaptaci virtuální veže pro dámu
     * @param isBlack   True - černá, False - bílá
     */
    void setIsBlack(bool isBlack);
};

#endif // VEZ_H
