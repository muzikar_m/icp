//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include <model/field.h>
#include <set>
#include "pesec.h"
#include "model/board.h"

bool Pesec::canMove(Address address) {
    int i = address.row - this->getRow();
    int j = address.col - this->getCol();

    if ((i == 0) && (j == 0)) {
        return false;
    }

    int smer = 1;
    // TODO je to spravne? jsou cerni nahore? kdyzak odstranit negaci podminky
    if (this->isBlack()) {
        smer = -1;
    }

    int x = address.row;
    int y = address.col;

    if (x < 0 || x > 7 || y < 0 || y > 7) {
        return false;
    }

    // 1 krok dopredu
    if ((i == smer) && (j == 0) && (this->getField(x,y)->isEmpty())) {
        return true;
    }

    // 2 kroky dopredu (&& musi byt i o jedno dopredu volno (o jedno pred cilovym).)
    if (((i == 2*smer) && (j == 0) && (this->getField(x,y)->isEmpty())) &&
        ((this->getField(x-smer,y)->isEmpty()))){
        // prvni tah -> tah z vychozi pozice
        if ((this->isBlack()) && (this->getRow() == 6)) {
            return true;
        }
        if ((!(this->isBlack())) && (this->getRow() == 1)) {
            return true;
        }
    }

    // krok diagonalne
    std::set <int> col = {-1,1};
    if ((i == smer) && (col.count(j)) && (!(this->getField(x,y)->isEmpty()))) {
        // budu brat (mozna)
        if (this->isBlack()) {
            if (this->getField(x,y)->getPiece()->isBlack()) {
                return false;
            }
        } else {
            if (!(this->getField(x,y)->getPiece()->isBlack())) {
                return false;
            }
        }
        return true;
    }

    return false;
}

std::vector<all_moves> Pesec::allMoves() {
    std::vector<all_moves> moves;
    all_moves one_move;

    int smer = 1;
    if (this->isBlack()) {
        smer = -1;
    }

    // tah o 1 dopredu
    int x = this->getRow() + smer;
    int y = this->getCol();
    one_move.address.row = x;
    one_move.address.col = y;
    if (this->canMove(one_move.address)) {
        one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
        moves.push_back(one_move);
    }

    // tah o 2 dopredu
    x = this->getRow() + smer*2;
    y = this->getCol();
    one_move.address.row = x;
    one_move.address.col = y;

    if (this->canMove(one_move.address)) {
        one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
        moves.push_back(one_move);
    }

    // tah diagonalne vpravo
    x = this->getRow() + smer;
    y = this->getCol() + 1;
    one_move.address.row = x;
    one_move.address.col = y;

    if (this->canMove(one_move.address)) {
        one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
        moves.push_back(one_move);
    }

    // tah diagonalne vlevo
    x = this->getRow() + smer;
    y = this->getCol() - 1;
    one_move.address.row = x;
    one_move.address.col = y;

    if (this->canMove(one_move.address)) {
        one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
        moves.push_back(one_move);
    }

    return moves;
}

PieceType Pesec::getType() {
    return PieceType::PESEC;
}

char Pesec::GetPieceChar() {
    if (this->isBlack()) {
        return 'o';
    }
    return 'p';
}
