//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include <model/field.h>
#include "strelec.h"
#include <model/board.h>

bool Strelec::canMove(Address address) {

    int x = address.row;
    int y = address.col;

    int i = address.row - this->getRow();
    int j = address.col - this->getCol();

    if ((i == 0) && (j == 0)) {
        return false;
    }

    // kotrola diagonalniho pohybu
    if (abs(i) != abs(j)) {
        return false;
    }

    if (x < 0 || x > 7 || y < 0 || y > 7) {
        return false;
    }

    // kontrola prazdnosti cesty
    int smer_x, smer_y;
    if (i < 0) {
        smer_x = 1;
    } else {
        smer_x = -1;
    }

    if (j < 0) {
        smer_y = 1;
    } else {
        smer_y = -1;
    }

    //for (i += smer; i != 0; i += smer)
    i += smer_x;
    j += smer_y;
    while ((i != 0) && (j != 0)) {
        std::cout << "i " << i  << " j " << j << " vez" << std::endl;
        x = this->getRow() + i;
        y = this->getCol() + j;
        if (!(this->getField(x,y)->isEmpty())) {
            return false;
        }
        i += smer_x;
        j += smer_y;
    }
     std::cout << "--------";

    x = address.row;
    y = address.col;

    // budu brat (mozna)
    if (!(this->getField(x,y)->isEmpty())){
        if (this->isBlack()) {
            if (this->getField(x,y)->getPiece()->isBlack()) {
                return false;
            }
        } else {
            if (!(this->getField(x,y)->getPiece()->isBlack())) {
                return false;
            }
        }
    }

    return true;
}

std::vector<all_moves> Strelec::allMoves() {
    std::vector<all_moves> moves;
    all_moves one_move;
    // kontrola na hlavni diagonale
    for (int i = -7; i <= 7; i++) {
        int x = this->getRow() + i;
        int y = this->getCol() + i;
        if (x >= 0 && x <= 7 && y >= 0 && y <= 7) {
            one_move.address.row = x;
            one_move.address.col = y;
            if (this->canMove(one_move.address)) {
                one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
                moves.push_back(one_move);
            }
        }
    }

    // kontrola vedlejsi diagonale
    for (int i = -7; i <= 7; i++) {
        int x = this->getRow() + i;
        int y = this->getCol() - i;

        one_move.address.row = x;
        one_move.address.col = y;
        if (this->canMove(one_move.address)) {
            one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
            moves.push_back(one_move);
        }
    }
    return moves;
}

PieceType Strelec::getType() {
    return PieceType::STRELEC;
}


char Strelec::GetPieceChar() {
    if (this->isBlack()) {
        return 'v';
    }
    return 'b';
}
