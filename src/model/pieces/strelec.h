//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef STRELEC_H
#define STRELEC_H

#include <QObject>
#include <QWidget>
#include <model/piece.h>

class Strelec : public Piece {
public:
    using Piece::Piece;
    /**
     * @brief Ověří validitu pohybu střelce
     * @param address adresa, kam se chci přesunout
     * @return True pokud je to validní pohyb, jinak false
     */
    bool canMove(Address address) override;
    /**
     * @brief Vrací typ dané figurky
     * @return typ figurky: střelec
     */
    PieceType getType() override;
    /**
     * @brief Vrací znak ve fontu, který značí střelce
     * @return znak střelce
     */
    char GetPieceChar() override;
    /**
     * @brief Zjistí, kam se všude může střelec aktuálně pohybovat.
     * @return pole všech pohybů
     */
    std::vector<all_moves> allMoves() override;
};

#endif // STRELEC_H
