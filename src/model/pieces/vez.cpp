//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include <model/field.h>
#include "vez.h"
#include <model/board.h>

bool Vez::canMove(Address address) {
    int i = address.row - this->getRow();
    int j = address.col - this->getCol();

    if ((i == 0) && (j == 0)) {
        return false;
    }

    // jde po radku nebo po sloupci
    if (!((i == 0) || (j == 0))) {
        return false;
    }

    int x = address.row;
    int y = address.col;

    if (x < 0 || x > 7 || y < 0 || y > 7) {
        return false;
    }

    int smer;
    bool go_row = true;

    // jdem po radku
    if (i == 0) {
    // jdem po slopcu
        go_row = false;
        i = j;
    }

    // urceni smeru
    if (i < 0) {
        smer = 1;
    } else {
        smer = -1;
    }

    for (i += smer; i != 0; i += smer) {
        // radek
        if (go_row) {
            x = this->getRow() + i;
            y = this->getCol();
            if (!(this->getField(x,y)->isEmpty())) {
                return false;
            }
        // slpec
        } else {
            x = this->getRow();
            y = this->getCol() + i;
            std::cout << std::endl << "i " << i << "  y " << y << std::endl;
            if (!(this->getField(x,y)->isEmpty())) {
                return false;
            }
        }
    }

    x = address.row;
    y = address.col;

    // budu brat (mozna)
    if (!(this->getField(x,y)->isEmpty())){
        if (this->isBlack()) {
            if (this->getField(x,y)->getPiece()->isBlack()) {
                return false;
            }
        } else {
            if (!(this->getField(x,y)->getPiece()->isBlack())) {
                return false;
            }
        }
    }

    return true;
}

std::vector<all_moves> Vez::allMoves() {
    std::vector<all_moves> moves;
    all_moves one_move;
    // kontrola diagonalne
    for (int i = -7; i <= 7; i++) {
        std::cout << "i=" << i << " | ";
        int x = this->getRow() + i;
        int y = this->getCol();

        std::cout << "x=" << this->getRow() << "->" << x << " y=" << this->getCol() << "->" << y << std::endl;
        one_move.address.row = x;
        one_move.address.col = y;
        if (this->canMove(one_move.address)) {
            one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
            moves.push_back(one_move);
            std::cout << " <<<<<<<<< cajk" << std::endl;
        }

        // kontrola vertikalne
        x = this->getRow();
        y = this->getCol() + i;
        one_move.address.row = x;
        one_move.address.col = y;
        if (this->canMove(one_move.address)) {
            one_move.type = (this->board->getCell(x,y)->isEmpty()) ? MoveType::PEACE : MoveType::KILL;
            moves.push_back(one_move);
        }

    }
    std::cout << std::endl << std::endl;
    return moves;
}

void Vez::setIsBlack(bool isBlack)
{
    this->m_isBlack = isBlack;
}

PieceType Vez::getType(){
    return PieceType::VEZ;
}

char Vez::GetPieceChar() {
    if (this->isBlack()) {
        return 't';
    }
    return 'r';
}
