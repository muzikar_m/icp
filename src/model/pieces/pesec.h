//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef PESEC_H
#define PESEC_H

#include <QObject>
#include <QWidget>
#include <model/piece.h>

class Pesec : public Piece {
public:
    using Piece::Piece;
    /**
     * @brief Ověří validitu pohybu pěšce
     * @param address adresa, kam se chci přesunout
     * @return True pokud je to validní pohyb, jinak false
     */
    bool canMove(Address address) override;
    /**
     * @brief Vrací typ dané figurky
     * @return typ figurky: pěšec
     */
    PieceType getType() override;
    /**
     * @brief Vrací znak ve fontu, který značí pěšce
     * @return znak pěšce
     */
    char GetPieceChar() override;
    /**
     * @brief Zjistí, kam se všude může pěšec aktuálně pohybovat.
     * @return pole všech pohybů
     */
    std::vector<all_moves> allMoves() override;
};

#endif // PESEC_H
