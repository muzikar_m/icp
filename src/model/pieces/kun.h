//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef KUN_H
#define KUN_H

#include <QObject>
#include <QWidget>
#include <model/piece.h>

class Kun : public Piece{
public:
    using Piece::Piece;
    /**
     * @brief Ověří validitu pohybu koně
     * @param address adresa, kam se chci přesunout
     * @return True pokud je to validní pohyb, jinak false
     */
    bool canMove(Address address) override;
    /**
     * @brief Vrací typ dané figurky
     * @return typ figurky: kůň
     */
    PieceType getType() override;
    /**
     * @brief Vrací znak ve fontu, který značí koně
     * @return znak koně
     */
    char GetPieceChar() override;
    /**
     * @brief Zjistí, kam se všude může kůň aktuálně pohybovat.
     * @return pole všech pohybů
     */
    std::vector<all_moves> allMoves() override;
};

#endif // KUN_H
