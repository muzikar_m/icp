//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef KRAL_H
#define KRAL_H

#include <QObject>
#include <QWidget>
#include <model/piece.h>

class Kral : public Piece {
public:
    using Piece::Piece;
    /**
     * @brief Ověří validitu pohybu krále
     * @param address adresa, kam se chci přesunout
     * @return True pokud je to validní pohyb, jinak false
     */
    bool canMove(Address address) override;
    /**
     * @brief Vrací typ dané figurky
     * @return typ figurky: král
     */
    PieceType getType() override;
    /**
     * @brief Vrací znak ve fontu, který značí krále
     * @return znak krále
     */
    char GetPieceChar() override;
    /**
     * @brief Zjistí, kam se všude může král aktuálně pohybovat.
     * @return pole všech pohybů
     */
    std::vector<all_moves> allMoves() override;
};

#endif // KRAL_H
