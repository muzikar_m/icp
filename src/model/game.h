//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef GAME_H
#define GAME_H

#include <string>
#include <model/board.h>
#include <utils/parser.h>

class Game
{
    /**
     * @brief Deska kterou hra disponuje
     */
    Board board;
    /**
     * @brief Historie (i budoucnost) všech pohybů co se na desce udály
     */
    std::vector<Move> moves;
    /**
     * @brief Index, kde se v poli všech pohybů právě nacházíme
     */
    int currentMove;
    /**
     * @brief Proměnná určující kdo je na tahu
     */
    bool isBlacksTurn;
    /**
     * @brief Vrací všechny figurky co jsou ještě živé na desce
     * @param isBlack chci černé (True) nebo černé (False)?
     * @return pole figurek
     */
    std::vector<Piece *> getAllPieces(bool isBlack);
    /**
     * @brief Vrací pole všech možných pohybů hráče daného argumentem
     * @param isPlayingBlack získá figurky protihráče (získá figurky bílého - true)
     * @return pole všech možných pohybů
     */
    std::vector<all_moves> getAllPossibleMoves(bool isPlayingBlack);
    /**
     * @brief Vrací pole všech dosažitelných políček protihráčem
     * @param isPlayingBlack Hráč který je na tahu (True - černý)
     * @return pole vsech políček
     */
    std::vector<Address> getAllReachableFields(bool isPlayingBlack);
    /**
     * @brief Vrací krále na desce
     * @param isBlack chce černého (True) nebo bílého?
     * @return figurka Krále
     */
    Piece *getKing(bool isBlack);
public:
    Game();
    /**
     * @brief Inicializace hry
     * @param filepath cesta k souboru
     */
    void initialize(const std::string& filepath);
    /**
     * @brief Přesune figurku
     * @param move Pohyb obsahující figurku, odkud a kam
     */
    void doMove(Move move);
    /**
     * @brief Provede tah zpět v historii tahů
     */
    void undo();
    /**
     * @brief Provede tah do "budoucnosti" v histoii tahů
     */
    void redo();
    /**
     * @brief Vrací desku, kteoru hra disponuje
     * @return hrací deska
     */
    Board *getBoard();
    //TODO musi tohle vsechno byt public?
    /**
     * @brief Vrací celou historii pohybů
     * @return pole pohybů
     */
    std::vector<Move> getMoves();
    /**
     * @brief získá políčka ohrozené protihráčem
     * @param isPlayingBlack Hráč který je na tahu (True - černý)
     * @return pole pohybů
     */
    std::vector<Address> getRedFileds(bool isPlayingBlack);
    /**
     * @brief Zjistí, jestli je král v šachu
     * @param isPlayingBlack barva zjištovaného krále
     * @return True je li ohrožen
     */
    bool sach(bool isPlayingBlack);
    /**
     * @brief uloží danou hru
     * @param out kam
     */
    void saveGame(std::ofstream& out);
    /**
     * @brief vrátí index, kde se nacházíme v poli pohybů
     * @return index v poli pohybů
     */
    int getCurrentMove();
    /**
     * @brief Vrací barvu hráče, který je na tahu
     * @return vrací True pokud hraje černý
     */
    bool isBlackTurn();
    /**
     * @brief nastavuje index, kde se v poli tahů nacházíme
     * @param move kam se přesunout
     */
    void setCurrentMove(int move);
};

#endif // GAME_H
