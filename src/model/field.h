//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef CELL_H
#define CELL_H

#include <model/piece.h>

class Board;

class Field
{
    /**
     * @brief Řádek ve kterém se na desce nachází
     */
    int row;
    /**
     * @brief Sloupec ve kterém se na desce nachází
     */
    int col;
    /**
     * @brief Deska, do které patří
     */
    Board *parent;
    /**
     * @brief Figurka která na políčku stojí
     */
    Piece *piece;
public:
    /**
     * @brief Konstruktor
     */
    Field();
    /**
     * @brief Inicializace daného políčka
     * @param parent deska do které políčko spadá
     * @param row řádek na desce, ve kterém se nachází
     * @param col sloupec na desce, ve kterém se nachází
     */
    void initialize(Board *parent, int row, int col);
    /**
     * @brief funkce vrací barvu figurky, která stojí na políčku
     * @return True pokud je bílá, False pokud je černá
     */
    bool isWhite();
    /**
     * @brief hasPiece Vrací prázdnost políčka
     * @return True pokud je plné, False pokud prázdné
     */
    bool hasPiece();
    /**
     * @brief Vrací figurku co stojí na políčku.
     * @return figurka
     */
    Piece *getPiece();
    /**
     * @brief Postaví figurku na dané políčko
     * @param piece Figurka, kterou chceme postavit
     */
    void setPiece(Piece *piece);
    /**
     * @brief Odstraní figurku z daného políčka
     */
    void killPiece();
    /**
     * @brief Test prázdnosti políčka
     * @return Vrací True pokud je políčko prázdné, False pokud ne
     */
    bool isEmpty();
    /**
     * @brief Vrací adressu daného políčka
     * @return adresa
     */
    Address getAddress();
};

#endif // CELL_H
