//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "piece.h"
#include <model/board.h>
#include <model/pieces/dama.h>
#include <model/pieces/kun.h>
#include <model/pieces/vez.h>
#include <model/pieces/kral.h>
#include <model/pieces/strelec.h>
#include <model/pieces/pesec.h>

int Piece::getRow(){
    return this->address.row;
}

int Piece::getCol(){
    return this->address.col;
}


Field *Piece::getField(int x, int y){
    return this->board->getCell(x,y);
}

Field *Piece::getMyField(){
    return this->board->getCell(this->address.row,this->address.col);
}

void Piece::setBoard(Board *board)
{
    this->board = board;
}

bool Piece::isBlack(){
    return m_isBlack;
}

void Piece::setAddress(Address address)
{
    this->address = address;
}

Address Piece::getAddress () {
    return this->address;
}

Board* Piece::getBoard() {
    return this->board;
}

Piece *Piece::makePiece(PieceType type, bool isBlack, Board *board){
    switch(type){
    case PieceType::KRAL:
        return new Kral(isBlack, board);
    case PieceType::DAMA:
        return new Dama(isBlack, board);
    case PieceType::VEZ:
        return new Vez(isBlack, board);
    case PieceType::KUN:
        return new Kun(isBlack, board);
    case PieceType::STRELEC:
        return new Strelec(isBlack, board);
    case PieceType::PESEC:
        return new Pesec(isBlack, board);
    }
    //TODO pridat implementace
    return nullptr;
}

