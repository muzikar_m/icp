//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef PIECE_H
#define PIECE_H

#include <model/address.h>
#include <vector>
class Field;

typedef enum {
    KRAL = 'K',
    DAMA = 'D',
    VEZ = 'V',
    STRELEC = 'S',
    KUN = 'J',
    PESEC = 'p'
} PieceType;

typedef enum {
    BLACK,
    WHITE
} PieceColor;

typedef enum {
    KILL,
    PEACE
} MoveType;

/**
 * @brief struktura jednoho pohybu s jeho typem
 */
struct all_moves {
    Address address;
    MoveType type;
};

class Board;

/**
 * @brief The Piece class
 */
class Piece {
    protected:
        /**
         * @brief Adresa figurky
         */
        Address address;
        /**
         * @brief Barva figurky (True - černá)
         */
        bool m_isBlack;
        /**
         * @brief Deska, do které figurka patří
         */
        Board *board;
    public:
        /**
         * @brief Konstruktor figurky
         * @param isBlack barva figurky
         * @param board deske, do které figurka patří
         */
        Piece(bool isBlack, Board *board) : m_isBlack(isBlack), board(board) {}
        /**
         * @brief Vrací barvu figurky
         * @return True pokud je černá
         */
        bool isBlack();
        /**
         * @brief Test pohybu fihgurky na dané pole
         * @param address pole na které testujeme přesun
         * @return True pokud byl pohyb validní
         */
        virtual bool canMove(Address address) = 0;
        /**
         * @brief Vrací všechny pohyby, které může figurka vykonat
         * @return pole Adress a typu pohybu
         */
        virtual std::vector<all_moves> allMoves() = 0;
        /**
         * @brief Vrací typ figurky
         * @return typ figurky
         */
        virtual PieceType getType() = 0;
        /**
         * @brief Vrací znak reprezentující figurku ve fontu Chess7
         * @return znak dané figurky
         */
        virtual char GetPieceChar() = 0;
        /**
         * @brief Nastavuje adresu dané figurky
         * @param address adresa kam přesouváme figurku
         */
        void setAddress(Address address);
        /**
         * @brief Vytvoří novou figurku
         * @param type typ figurky
         * @param isBlack barva figurky (true - černá)
         * @param board deska, ve které se figurka vytváří
         * @return vytvořená figurka
         */
        static Piece *makePiece(PieceType type, bool isBlack, Board *board);
        /**
         * @brief Frací pole na daných souřadnicích v desce, ve které se figurka nachází
         * @param x radek
         * @param y sloupec
         * @return žádáné pole
         */
        Field *getField(int x, int y);
        /**
         * @brief Vrací políčko, na kterm se figurka nachází
         * @return Políčko
         */
        Field *getMyField();
        /**
         * @brief Natavuje desku, na které se figurka nachází
         * @param board daná deska
         */
        void setBoard(Board *board);
        /**
         * @brief Vrací index řádku, na kterém se figurka nachází
         * @return index řádku
         */
        int getRow();
        /**
         * @brief Vrací index sloupce, na kterém se figurka nachází
         * @return index sloupce
         */
        int getCol();
        /**
         * @brief Vrací adresu políčka, na kterém se figurka nachází
         * @return adresa políčka
         */
        Address getAddress();
        /**
         * @brief
         * @brief Vrací desku, na které se figurka nachází
         * @return deska
         */
        Board* getBoard();
};


#endif // PIECE_H
