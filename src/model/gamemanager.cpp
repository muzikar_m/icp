//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "gamemanager.h"

#include <iostream>

GameManager::GameManager()
{
}

GameManager *GameManager::getManager(){
    static GameManager *manager = new GameManager();
    return manager;
}

Game *GameManager::getGame(int i){
    return this->games[i];
}

void GameManager::setCurrentGame(int i)
{
    currentGame = i;
}

Game *GameManager::createGame(int& index){
    this->games.push_back(new Game());
    index = games.size() - 1;
    return this->games.back();
}

Game *GameManager::getCurrentGame()
{
    return this->games[currentGame];
}
