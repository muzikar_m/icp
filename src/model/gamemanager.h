//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <vector>
#include <model/game.h>

/**
 * @brief The GameManager class
 */
class GameManager
{
    /**
     * @brief konstruktor
     */
    GameManager();
    /**
     * @brief pole všech her
     */
    std::vector<Game*> games;
    /**
     * @brief index aktuálně zobrazované hry
     */
    int currentGame = 0;
public:
    /**
     * @brief vytvoří henrího manažera
     * @return herní manažer
     */
    static GameManager *getManager();
    /**
     * @brief vytvoří novou hru
     * @param index přes který se vrací index hry
     * @return vytvořená hra
     */
    Game *createGame(int& index);
    /**
     * @brief Vrací aktuálně zobrazovanou hru
     * @return zobrazovaná hra
     */
    Game *getCurrentGame();
    /**
     * @brief Vrací hru na daném indexu
     * @param i index kterou hru chceme získat
     * @return hra na indexu
     */
    Game *getGame(int i);
    /**
     * @brief Změní aktuálně zobrazovanou hru
     * @param i index, na kterou hru chceme měnit
     */
    void setCurrentGame(int i);
    /**
     * @brief Odstraní hru z pole her
     * @param i index, kteoru hru odstraňujeme
     */
    void removeGame(int i);
};

#endif // GAMEMANAGER_H
