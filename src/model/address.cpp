//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "address.h"

Address Address::getAddres(char col, int row)
{
    if (col < 'A' || col > 'F'){
        throw "Spatny sloupec";
    }
    return Address('H' - (7 - col - 1), row - 1);
}


//bool operator==(const Address &druha) const
bool Address::operator== (const Address &druha){
    return (this->col == druha.col && this->row == druha.row);
}
