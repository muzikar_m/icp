//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef BOARD_H
#define BOARD_H

class Game;

#include <model/piece.h>
#include <model/field.h>
#include <array>
#include <model/move.h>

/**
 * @brief Třída reprezentující hrací desku
 */
class Board
{
    /**
     * @brief Konstanta velikosti desky
     */
    static const int BOARD_SIZE = 8;

    /**
     * @brief pole všech políček na desce
     */
    std::array<Field, BOARD_SIZE * BOARD_SIZE> cells;
    /**
     * @brief pole všech figurek na desce
     */
    std::array<Piece *, 4 * BOARD_SIZE> pieces;
    /**
     * @brief Dosadí na řádek speciální figurky (ostatní než je pěšec)
     * @param i čislo řádku, kam je chceme dosafit
     * @param isBlack   True pokud chceme černé, False pokud bílé
     */
    void initializeRow(int i, bool isBlack);
    /**
     * @brief ulozi do pomocného seznamu figurek figurku a vrati ji pro dalsi pouziti
     * @param type  typ dané figurky
     * @param isBlack   barva dané figurky
     * @param b deska Board, se kerou pracujeme
     * @return vrací figurku, kterou jsme právě vložili
     */
    Piece *cachePiece(PieceType type, bool isBlack, Board *b);

    Game* game;

    /**
     * @brief Počítadlo figurek
     */
    int pieceCounter;
public:
    /**
     * @brief konstruktor Desky
     * @param game  hra, ke které deska patří
     */
    Board(Game *game);
    /**
     * @brief Získá políčko na téhle desce
     * @param row   řádek na desce
     * @param col   sloupec na desce
     * @return políčko na desce
     */
    Field *getCell(int row, int col);
    /**
     * @brief Získá políčko na téhle desce
     * @param a Adresa požadovaného políčka
     * @return políčko na desce
     */
    Field *getCell(Address a);
    /**
     * @brief Získá políčko na téhle desce
     * @param index v jednorozměrném poli reprezentujícím pole políček na desce
     * @return políčko na desce
     */
    Field *getCell(int index);
    /**
     * @brief Zkontroluje validitu pohybu
     * @param move  Pohyb, který chceme zkontrolovat
     * @return True v případě validního pohybu, jinak false
     */
    bool isMoveValid(Move move);
    /**
     * @brief Vrací všchny políčka na dané desce
     * @return pole políček
     */
    std::array<Piece*, 4 * BOARD_SIZE>& getAllPieces();
    /**
     * @brief Vrací hru, ke které deska patří
     * @return hra
     */
    Game * getGame();
};

#endif // BOARD_H
