//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef ADDRESS_H
#define ADDRESS_H

#include <string>
#include <iostream>

/**
 * @brief Třída Address
 */
class Address
{
private:
public:
    /**
     * @brief col   sloupec
     * @brief row   řádek
     */
    int col, row;
    Address(int col = 0, int row = 0): col(col), row(row) {}
    static bool isShortNotation(std::string str);
    static bool isLongNotation(std::string str);
    static Address getAddres(char col, int row);
    static Address getFromNotation(std::string str);

    /**
     * @brief Přetížení operátoru <<
     * @details Uloží do proměnné out adresu a ve tvaru (row,col)
     * @param out proměnná do které ukládáme výsledek
     * @param a adresa
     * @return vrací adresu v daném formátu.
     */
    friend std::ostream& operator<<(std::ostream& out, const Address& a){
        out << '(' << a.row << ',' << (char)a.col << ')';
        return out;
    }

    /**
     * @brief Přetízení operátoru ==
     * @param druha porovnávaná adresa.
     * @return True pokud se adresy rovnají, jinak False.
     */
    bool operator== (const Address &druha);
};

#endif // ADDRESS_H
