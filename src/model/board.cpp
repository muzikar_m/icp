//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "board.h"
#include <iostream>


Piece *Board::cachePiece(PieceType type, bool isBlack, Board *b){
    Piece *p = Piece::makePiece(type, isBlack, b);
    pieces[pieceCounter++] = p;
    return p;
}

void Board::initializeRow(int i, bool isBlack){
    getCell(i, 0)->setPiece(cachePiece(PieceType::VEZ, isBlack, this));
    getCell(i, BOARD_SIZE-1)->setPiece(cachePiece(PieceType::VEZ, isBlack, this));

    getCell(i, 1)->setPiece(cachePiece(PieceType::KUN, isBlack, this));
    getCell(i, BOARD_SIZE-2)->setPiece(cachePiece(PieceType::KUN, isBlack, this));

    getCell(i, 2)->setPiece(cachePiece(PieceType::STRELEC, isBlack, this));
    getCell(i, BOARD_SIZE-3)->setPiece(cachePiece(PieceType::STRELEC, isBlack, this));

    getCell(i, 3)->setPiece(cachePiece(PieceType::KRAL, isBlack, this));
    getCell(i, BOARD_SIZE-4)->setPiece(cachePiece(PieceType::DAMA, isBlack, this));
}

Board::Board(Game *game) : game(game){
    pieceCounter = 0;
    for (int i = 0; i < BOARD_SIZE; i++){
        for (int j = 0; j < BOARD_SIZE; j++){
            getCell(i,j)->initialize(this, i,j);
        }
    }
    std::flush(std::cout);
    for (int i = 0; i < BOARD_SIZE; i++) {
        getCell(1, i)->setPiece(cachePiece(PieceType::PESEC, false, this));
        getCell(BOARD_SIZE-2, i)->setPiece(cachePiece(PieceType::PESEC, true, this));
    }
    std::flush(std::cout);
    this->initializeRow(0, false);
    this->initializeRow(BOARD_SIZE-1, true);
    std::flush(std::cout);
}

Field *Board::getCell(int row, int col){
    return &(this->cells[row * BOARD_SIZE + col]);
}

Field *Board::getCell(Address a)
{
    return getCell(a.row, a.col);
}

Field *Board::getCell(int index)
{
    return &(this->cells[index]);
}

bool Board::isMoveValid(Move move)
{
    Field *f = getCell(move.getFrom());
    if (!f->hasPiece()){
        return false;
    }
    return f->getPiece()->canMove(move.getTo());
}

std::array<Piece *, 4 * Board::BOARD_SIZE> &Board::getAllPieces()
{
    return pieces;
}


Game *Board::getGame() {
   return this->game;
}
