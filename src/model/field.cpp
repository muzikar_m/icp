//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "field.h"
#include <iostream>

Field::Field() {

}

void Field::initialize(Board *parent, int row, int col) {
    this->parent = parent;
    this->row = row;
    this->col = col;
    this->piece = nullptr;
}

bool Field::hasPiece()
{
    return this->piece != nullptr;
}

Piece *Field::getPiece()
{
    return this->piece;
}

void Field::setPiece(Piece *piece) {
    // TODO vyhazovani figurek
    this->piece = piece;
    if (piece != nullptr){
        piece->setAddress(Address(col, row));
    }
}

bool Field::isEmpty() {
    return (this->piece == nullptr) ? true : false;
}

Address Field::getAddress()
{
    return Address(col, row);
}
