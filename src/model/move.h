//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef MOVE_H
#define MOVE_H

#include <model/address.h>
#include <model/piece.h>

#include <iostream>

class Move
{
private:
    /**
     * @brief Adresa pole, odkud se hýbe
     */
    Address from;
    /**
     * @brief Ukazatel na figurku, se kterou budemem pohybovat
     */
    Piece *pfrom;
    /**
     * @brief Adresa pole, kam se hýbe
     */
    Address to;
    /**
     * @brief Ukazatel na figurku, která bude odstraněna
     */
    Piece *pto;
public:
    /**
     * @brief konstruktor pohybu
     * @param from pole odkud
     * @param pfrom figurka odkud
     * @param to pole kam
     * @param pto figurka kam
     */
    Move(Address from, Piece *pfrom, Address to, Piece *pto):
        from(from), pfrom(pfrom), to(to), pto(pto) {}
    /**
     * @brief Vrací validitu pohybu
     * @return True pokud je validní
     */
    bool isValid();
    /**
     * @brief Vytvoří pohyb figurky z políčka f na políčko t
     * @param f políčku odkud přesuváme
     * @param t políčko kam přesouváme
     */
    static Move fromTo(Field *f, Field *t);
    /**
     * @brief Vytvoří invalidní pohyb
     */
    static Move createInvalid();
    /**
     * @brief Vrací pole, ze kterého se pohybujeme
     * @return políčko odkud
     */
    Address getFrom();
    /**
     * @brief Vrací pole, kam se pohybujeme
     * @return políčko kam
     */
    Address getTo();
    /**
     * @brief Vrací figurku, se kterou se pohybujeme
     * @return figurka se kterou se pohybuje
     */
    Piece *getPiece();
    /**
     * @brief Vrací plnou nataci daného pohybu
     * @return Zápis notace tahu
     */
    std::string getFullNotation();
    /**
     * @brief Přetížení operátoru <<, vypíše pohyb na stream out
     * @param out stream kam budem vypisovat
     * @param m pohyb k výpisu
     * @return vrací stream na který se vypisuje
     */
    friend std::ostream& operator<<(std::ostream& out, const Move& m){
        out << m.from << m.to;
        return out;
    }
};

#endif // MOVE_H
