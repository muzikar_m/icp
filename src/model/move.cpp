//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "move.h"

#include <model/field.h>

bool Move::isValid()
{
    return from.col != -1 || from.row != -1;
}

Move Move::fromTo(Field *f, Field *t)
{
    return Move(f->getAddress(), f->getPiece(), t->getAddress(), t->getPiece());
}

Move Move::createInvalid()
{
    return Move(Address(-1, -1), nullptr, Address(), nullptr);
}


Address Move::getFrom()
{
    return from;
}

Address Move::getTo(){
    return to;
}

Piece *Move::getPiece(){
    return pto;
}

std::string Move::getFullNotation()
{
    char notation[20] = {0, };
    std::sprintf(notation, "%c%c%d%c%d",
                 this->pfrom->getType(), from.col + 'a', from.row + 1, to.col + 'a', to.row + 1);
    return std::string(notation);
}
