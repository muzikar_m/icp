//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef TESTWINDOW_H
#define TESTWINDOW_H

#include <QMainWindow>
#include <model/game.h>
#include <QPushButton>
#include <QListWidget>

namespace Ui {
class testwindow;
}

class testwindow : public QMainWindow
{
    Q_OBJECT

    ///Tlacitka pouzita v sacohvnici
    QPushButton *gridButtons[8 * 8];
    ///Aktivni tlacitko v sachovnici
    QPushButton *activeButton;
    ///Font pouzity pro vykreslovani figurek
    QFont chessFont;
    ///Casovac pro prehravani her
    QTimer *timer;
public:
    /**
     * Funkce pro predpripraveni desky
     */
    void setupgrid();
    /**
     * Vykresleni hry
     * @param game aktualni hra
     */
    void drawGame(Game *game);
    /**
     * Vykresleni tahu do seznamu provedenych tahu
     * @param game aktualni hra
     */
    void drawUndos(Game *game);
    explicit testwindow(QWidget *parent = nullptr);
    ~testwindow();
private slots:
    /**
     * Provede jednou undo nad aktualni hrou
     */
    void on_pushButton_clicked();
    /**
     * Klik do hraci desky
     */
    void onGridClicked();

    /**
     * Callback pro prehravani hry
     */
    void onTimer();

    /**
     * Vyber aktivniho tahu
     * @param item tah
     */
    void on_undos_itemClicked(QListWidgetItem *item);

    void on_undos_currentRowChanged(int currentRow);

    /**
     * Jednou provede redo nad aktualni hrou
     */
    void on_pushButton_2_clicked();

    /**
     * Spousti/Vypina prehravani her
     */
    void on_pushButton_3_clicked();

    /**
     * Zmena rychlosti prehravani (pouziva se pro aktualizaci labelu)
     * @param value
     */
    void on_horizontalSlider_valueChanged(int value);

    /**
     * Nacteni hry ze souboru
     */
    void on_pushButton_6_clicked();

    /**
     * Vytvoreni nove hry
     */
    void on_pushButton_5_clicked();

    /**
     * Zmena aktualni hry
     * @param item aktualni hra
     */
    void on_gameList_itemClicked(QListWidgetItem *item);

    /**
     * Ulozeni aktualni hry
     */
    void on_pushButton_4_clicked();

protected:
    void resizeEvent(QResizeEvent *event) override;

private:
    Ui::testwindow *ui;
};

#endif // TESTWINDOW_H
