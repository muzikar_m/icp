//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <model/move.h>
#include <model/board.h>
#include <fstream>
#include <iostream>
#include <regex>

class Parser
{
private:
    /**
     * @brief stream s inputem
     */
    std::ifstream input;
    /**
     * @brief board deska, se kterou parser pracuje
     */
    Board *board;
    /**
     * @brief index v poli pohybů na aktuální pohyb
     */
    int currentMove;
    /**
     * @brief refex kontrolující plnou notaci šachu
     */
    static const std::regex fullNotationRegex;
    /**
     * @brief regex kontrolující zkrácenou notaci šachu
     */
    static const std::regex shortNotationRegex;
    /**
     * @brief regex kontrolující prázdný řádek
     */
    static const std::regex ignoreLine;
    /**
     * @brief Funkce zkotroluje řádek ze souboru, jetli vyhovuje dlouhé notaci
     * @param match uakzatel na výstup kontroly regexem
     * @return vrací dvojici pohybů načtenou z notace
     */
    std::pair<Move, Move> parseFullNotation(const std::smatch& match);
    /**
     * @brief Funkce zkotroluje řádek ze souboru, jetli vyhovuje dlouhé notaci
     * @param match uakzatel na výstup kontroly regexem
     * @return vrací dvojici pohybů načtenou z notace
     */
    std::pair<Move, Move> parseShortNotation(const std::smatch& match);
public:
    /**
     * @brief Konstruktor parseru
     * @param board nastavuje desku
     * @param filepath nastavuje cestu k souboru
     */
    Parser(Board *board, const std::string& filepath);
    /**
     * @brief Kontrola konce souboru
     * @return False pokud je konec souboru
     */
    bool hasNext();
    /**
     * @brief Zkontroluje další řádek souboru
     * @return Vrací dvojici načtených pohybů
     */
    std::pair<Move, Move> nextMove();
    ~Parser();
};

#endif // PARSER_H
