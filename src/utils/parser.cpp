//Projekt ICP duben 2019 
//Autori:	xrysav27 
//		xmuzik06 

#include "parser.h"
#include <regex>

const std::regex Parser::fullNotationRegex = std::regex("(\\d+). ([KDVSJp]?)([a-h])(\\d)([a-h])(\\d)(?: ([KDVSJp]?)([a-h])(\\d)([a-h])(\\d))?");
const std::regex Parser::shortNotationRegex = std::regex("(\\d+). ([KDVSJp]?)([a-h])(\\d)(?: ([KDVSJp]?)([a-h])(\\d))?");
const std::regex Parser::ignoreLine = std::regex("\\w*");

Parser::Parser(Board *board, const std::string &filepath){
    this->board = board;
    this->input.open(filepath);
    this->currentMove = 0;
}

//TODO u figurek kontrolovat canMove
std::pair<Move, Move> Parser::parseFullNotation(const std::smatch& match){
    int index = std::stoi(match[1]);
    if (index != this->currentMove){
        throw "Zadan spatny index pohybu";
    }
    char figure1 = 'p';
    if (match[2].str().length() != 0){
        figure1 = match[2].str().at(0);
    }
    char col1 = match[3].str().at(0) - 'a';
    int row1 = std::stoi(match[4]);
    Field *f1 = board->getCell(row1-1, col1);
    if (f1->isEmpty()){
        throw "Pohyb s figurkou na prazdnem poli";
    }
    Piece *p1 = f1->getPiece();
    if (p1->getType() != figure1){
        throw "Spatne oznacena figurka";
    }

    char col2 = match[5].str().at(0) - 'a';
    int row2 = std::stoi(match[6]);

    Field *f2 = board->getCell(row2-1, col2);

    //Notace pokracuje
    if (match[10].matched){
        char figure2 = 'p';
        if (match[7].str().length() != 0){
            figure2 = match[7].str().at(0);
        }
        char col3 = match[8].str().at(0) - 'a';
        int row3 = std::stoi(match[9]);
        Field *f3 = board->getCell(row3-1, col3);
        if (f3->isEmpty()){
            throw "Pohyb s figurkou na prazdnem poli";
        }
        if (f3->getPiece()->getType() != figure2){
            throw "Spatne oznacena figurka";
        }
        char col4 = match[10].str().at(0) - 'a';
        int row4 = std::stoi(match[11]);
        Field *f4 = board->getCell(row4-1, col4);


        return std::pair<Move, Move>(Move::fromTo(f1, f2), Move::fromTo(f3, f4));
    }

    return std::pair<Move, Move>(Move::fromTo(f1,f2), Move::createInvalid());

}

std::pair<Move, Move> Parser::parseShortNotation(const std::smatch &match)
{
    int index = std::stoi(match[1]);
    if (index != this->currentMove){
        throw "Zadan spatny index pohybu";
    }
    char figure1 = 'p';
    if (match[2].str().length() != 0){
        figure1 = match[2].str().at(0);
    }
    char col1 = match[3].str().at(0) - 'a';
    int row1 = std::stoi(match[4]);
    Address a1 = Address(row1-1, col1);
    Field *f1 = nullptr;
    for (auto& p : board->getAllPieces()){
        if (p->getType() == figure1){
            if (p->canMove(a1)){
                if (f1 != nullptr){
                    throw "Nejednoznacny zapis, vice figurek muze provest tento pohyb";
                }
                f1 = p->getMyField();
            }
        }
    }
    Field *f2 = board->getCell(row1-1, col1);
    if (match[7].matched){
        char figure2 = 'p';
        if (match[5].str().length() != 0){
            figure1 = match[5].str().at(0);
        }
        char col2 = match[6].str().at(0) - 'a';
        int row2 = std::stoi(match[7]);
        Address a2 = Address(row2-1, col2);
        Field *f2 = nullptr;
        for (auto& p : board->getAllPieces()){
            if (p->getType() == figure2){
                if (p->canMove(a2)){
                    if (f2 != nullptr){
                        throw "Nejednoznacny zapis, vice figurek muze provest tento pohyb";
                    }
                    f2 = p->getMyField();
                }
            }
        }
        Field *f3 = board->getCell(a2);
        return std::pair<Move, Move>(Move::fromTo(f1, f2), Move::fromTo(f2, f3));
    }
    return std::pair<Move, Move>(Move::fromTo(f1, f2), Move::createInvalid());
}

bool Parser::hasNext(){
    return !input.eof();
}

std::pair<Move, Move> Parser::nextMove(){
    std::string line;
    std::getline(this->input, line);
    this->currentMove++;
    std::smatch match;
    if (std::regex_match(line, match, Parser::fullNotationRegex)){
        return parseFullNotation(match);
    } else if (std::regex_match(line, match, Parser::shortNotationRegex)){
        return parseShortNotation(match);
    } else if (std::regex_match(line, Parser::ignoreLine)){
        return std::pair<Move,Move>(Move::createInvalid(), Move::createInvalid());
    }
    throw line + " nesplnuje ani jeden druh predpisu";
}

Parser::~Parser()
{
    input.close();
}
