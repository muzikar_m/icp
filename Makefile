# Makefile pro ICP projekt 2019
# Autori: Martin Muzikar (xmuzik06)
#         Tomas Rysavy   (xrysav26)
.DEFAULT: run

clean: clear

clear:
	rm -r build/

makefile: FORCE
	$(shell qmake-qt5 -Wnone -o build/Makefile src/chess.pro)

run: makefile
	cd build && $(MAKE) 
	$(shell build/chess)

doxygen:
	doxygen


pack:
	bash zip.sh

FORCE: ;
