#/usr/bin/fish

set HEADER '//Projekt ICP duben 2019 \n//Autori:\txrysav26 \n//\t\txmuzik06 \n\n';
set SCRIPT "1s,^,$HEADER,"; 

for f in src/**.cpp 
	sed -i $SCRIPT $f;
end;

for f in src/**.h 
	sed -i $SCRIPT $f;
end;
